# Local Airflow on Kubernetes

## Setup for local development

The following writeup should get you started on a local Airflow installation, running on Kubernetes,
with the KubernetesExecutor, loading its DagBag from your local directory `dags/` of this repository.

There are numerous components at play, most of which are abstracted away by running Airflow
packaged by Helm, and by having Docker Desktop maintain a local Kubernetes cluster.

Most notably, we make use of locally provisioned volumes, to inject DAG declarations into the running
Airflow installation.

### Prerequisistes

* Install Docker, and enable a local Kubernetes cluster

    In order to run Airflow locally, in combination with the KubernetesExecutor, you first must start
    yourself a local Kubernetes cluster to deploy Airflow into.

    This can be accomplished surprisingly quickly when using the [Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install/)
    or [Docker Desktop on Windows](https://docs.docker.com/docker-for-windows/install/) installations.
    Those come with an option to launch a local Kubernetes cluster.

    When you are working with Linux, you might want to have a look at [microk8s](https://microk8s.io/).

    **Note:** You can verify that step, with a couple of `kubectl` commands.

    ```
    $ kubectl get no
    NAME             STATUS   ROLES    AGE    VERSION
    docker-desktop   Ready    master   120m   v1.15.5
    ```

    ```
    $ kubectl auth can-i create pods
    yes
    ```

    If any of those fail you, you might want to revisit your Docker + Kubernetes setup.

* Check Kubernetes features

    When using Docker Desktop, you most likely have all features required already active
    in your Kubernetes cluster.
    
    When you are using microk8s, you might want to check post-installation steps.
    
    Make sure your Kubernetes cluster supports the following features:
    
    * StorageClass for PersistentVolumes and PersistentVolumeClaims
    * DNS

* Install Helm

    Another requirement to quickly get up and running, is Helm. Helm is like a package manager/deployment helper
    for Kubernetes. It allows you to quickly spin up a local Airflow installation.

    **Note:** It is recommended to use [Helm version <~ 2.16](https://github.com/helm/helm/releases/tag/v2.16.6)
              as opposed to the newer `3.x` versions. Much of the Airflow helm stuff benefits from
              some older version `2.x` features of Helm.

* Initialize Helm on your local Kubernetes cluster

    This step is important, as Helm requires Tiller, its pendant within the Kubernetes cluster to work.

    ```
    $ helm init
    ```

### Start your local Airflow installation

* Clone this repository

    ```
    $ git clone https://gitlab.com/Overbryd/kubernetes-airflow.git
    ```

* Setup the local volumes

    First you must find your path, that you have cloned the repository into.

    ```
    $ pwd
    /Users/lukas/Work/hibase-Idealo/airflow-test
    ```

    Take that path, and edit `local-airflow-pv.yml`, check the `TODO` and replace the given path `/tmp/dags` with
    the path where you cloned the repository into, appending the sub-path `/dags` to it.

    For example:

    ```
    hostPath:
      # TODO: change this to the absolute path of your repository here
      path: "/Users/lukas/Work/hibase-Idealo/airflow-test/dags"
    ```

    Now apply that declaration to your Kubernetes cluster.

    ```
    $ kubectl apply -f local-airflow-pv.yml
    ```

* Last, spin up a local Airflow installation

    ```
    $ helm upgrade --install --force --recreate-pods airflow --values airflow.yml stable/airflow
    ```

    Now you can verify your running Airflow installation with inspecting your Kubernetes cluster state:

    ```
    $ kubectl get deploy,po
    NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.extensions/airflow-flower      1/1     1            1           65s
    deployment.extensions/airflow-scheduler   1/1     1            1           65s
    deployment.extensions/airflow-web         0/1     1            0           65s

    NAME                                    READY   STATUS    RESTARTS   AGE
    pod/airflow-flower-76b4bd857f-crgk6     1/1     Running   0          65s
    pod/airflow-postgresql-0                1/1     Running   0          65s
    pod/airflow-redis-master-0              1/1     Running   0          65s
    pod/airflow-scheduler-6c566f6df-l2b82   1/1     Running   0          65s
    pod/airflow-web-5844ffddbc-m8jqp        0/1     Running   0          65s
    pod/airflow-worker-0                    1/1     Running   0          65s
    ```

    **Note:** It takes a considerable amount of time to spin up the web interface.
              Have some patience, it will be fully available after 3-5 minutes.

### Working with your local Airflow on Kubernetes

* Accessing the Airflow web interface

    In order to access the Airflow web interface running on Kubernetes, you have several options.
    The simplest is probably port forwarding to the pod running the web interface.

    Identify the pod running airflow-web:

    ```
    $ kubectl get po -lapp=airflow,component=web
    NAME                           READY   STATUS    RESTARTS   AGE
    airflow-web-5844ffddbc-m8jqp   1/1     Running   0          9m10s
    ```

    Take that pod, and forward port 8080 to it. Leave the following command running in the background.

    ```
    $ kubectl port-forward airflow-web-5844ffddbc-m8jqp 8080
    ```

    Now you should be able to access [http://localhost:8080/](http://localhost:8080/).

* Developing a DAG

    This repository hosts two example DAGs. Both are located in the folder `dags/`, and should be
    presented on the web interface.

    **Note:** Before you attempt any development, you should verify that both examples run to completion.
              Enable the "example_dag" and "example_kubernetes_pod", and trigger them at least once, manually.
              Check their runtime results and only proceed once they are running successful.

    Any DAG that you will place in the `dags/` directory, will be picked up by Airflow.
    That is because, your local `dags/` directory should be mounted as a PersitentVolume in the
    Airflow installation on Kubernetes.

    **Note:** Be careful when moving declarations around, or renaming files, that might break Airflow
              or prevent them from running. Safe yourself the headache, choose a name and stick with it,
              once you have loaded the DAG into the cluster.

